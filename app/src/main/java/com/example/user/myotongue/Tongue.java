package com.example.user.myotongue;

/**
 * Tongue object. It has 8 variables.
 */

public class Tongue {
    private int tid;
    private int level;
    private int leftPosition;
    private int rightPosition;
    private int upPosition;
    private int downPosition;
    private String startDate;
    private int stage;

    public Tongue(int level, int leftPosition, int rightPosition, int upPosition, int downPosition, String startDate, int stage) {
        this.level = level;
        this.leftPosition = leftPosition;
        this.rightPosition = rightPosition;
        this.upPosition = upPosition;
        this.downPosition = downPosition;
        this.startDate = startDate;
        this.stage = stage;
    }

    public Tongue(int tid, int level, int leftPosition, int rightPosition, int upPosition, int downPosition, String startDate, int stage) {
        this.tid = tid;
        this.level = level;
        this.leftPosition = leftPosition;
        this.rightPosition = rightPosition;
        this.upPosition = upPosition;
        this.downPosition = downPosition;
        this.startDate = startDate;
        this.stage = stage;
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLeftPosition() {
        return leftPosition;
    }

    public void setLeftPosition(int leftPosition) {
        this.leftPosition = leftPosition;
    }

    public int getRightPosition() {
        return rightPosition;
    }

    public void setRightPosition(int rightPosition) {
        this.rightPosition = rightPosition;
    }

    public int getUpPosition() {
        return upPosition;
    }

    public void setUpPosition(int upPosition) {
        this.upPosition = upPosition;
    }

    public int getDownPosition() {
        return downPosition;
    }

    public void setDownPosition(int downPosition) {
        this.downPosition = downPosition;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    @Override
    public String toString() {
        return "Tongue{" +
                "tid=" + tid +
                ", level=" + level +
                ", leftPosition=" + leftPosition +
                ", rightPosition=" + rightPosition +
                ", upPosition=" + upPosition +
                ", downPosition=" + downPosition +
                ", startDate='" + startDate + '\'' +
                ", stage=" + stage +
                '}';
    }
}
