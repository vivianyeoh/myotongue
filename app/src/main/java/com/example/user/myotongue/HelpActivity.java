package com.example.user.myotongue;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

/* This activity has information on how to use the app and details about the app. */

public class HelpActivity extends AppCompatActivity {
    public final static String howToPlay = "This game is about moving the tip of your tongue to 4 directions, up, down, left and right. " +
            "You must extend your tongue fully before the game start, and try not to put it back to your mouth during the game. " +
            "\n\nOnce you press start, you will see many empty circles on the right. Those circles record the number of ticks you need to do for that exercise. " +
            "Each stage requires 5 ticks for each exercise. To tick them, move your tongue to that direction. " +
            "\n\nOnce you have completed 5 ticks for every posture, you will proceed to the next stage. Try to pass as many stages as you can in 30 seconds! Have fun! ";
    public final static String aboutApp = "Sleeping plays critical role in human’s life. Lack of sleeping " +
            "affects the ability to think, alert, remember and process information in daily life. " +
            "Exhausted and drowsiness can cause serious health problem, or for worst, causes crashes, " +
            "injuries and fatalities. Lack of sleep has many factors. One of them is snoring and obstructive " +
            "sleep apnea. Snoring occur when disrupt airflow lead to vibration of oropharynx tissues, whereas " +
            "obstructive sleep apnea happens when upper airway partially or completely collapses and blocks the " +
            "ability to breathe and causes dropping in blood oxygen level and leads to sudden awakening from sleep. " +
            "These breathing problems disturb the sleep of people themselves and their sleep partner and affect " +
            "their life by being the cause them or their bed partner’s lack of sleep. " +
            "\n\nSome sleep specialists use Myofunctional therapy to improve breathing problems during sleep," +
            "especially in children. Myofunctional therapy is a program of specific exercises " +
            "that strengthen oropharynx muscle and reinforces proper position of tongue (the tip " +
            "of tongue should be placed against the hard palate behind front teeth). \n\nOropharynx is a " +
            "tube lined by muscular tissues around the mouth and throat. It keeps the airway open and plays " +
            "an important role in eating, talking and breathing. Weak oropharynx muscle creates " +
            "obstruction and disrupts the flow of air and causes many problems, for example, snoring " +
            "and obstructive sleep apnea. \n\nIn this application, motion tracking will be used to track the " +
            "user's tongue movements. Users are expected to complete at least 2 stages in 30 seconds " +
            "and ideally the entire set should be done at least 4 times per day. Myofunctional therapy should be performed " +
            "daily for at least 2 years to enjoy maximum benefit. It can be performed without additional " +
            "guidance, but professional assessment is also important to ensure maximum benefits. " +
            "This exercise may be used for users who are 6 years old or older. " +
            "The therapy can be done anywhere but it is suggested to be done in private to " +
            "avoid undesirable attention. \n\nThis therapy is proved to cure snoring and obstructive sleep " +
            "apnea to improve sleeping quality with a reduction in AHI of approximately 50% in adults and " +
            "62% in children. (The Apnea–Hypopnea Index or Apnoea–Hypopnoea Index (AHI) is an index used " +
            "to indicate the severity of sleep apnea). It is proved to be effective in children and " +
            "adults of all ages. It has also been shown to prevent relapse of sleep apnea after surgical " +
            "treatment. In an evaluation of the effect of oral exercise, two cases showed normalization of " +
            "after upper airway exercises and 7 cases showed more than 50% decrease in AHI but without " +
            "normalization of AHI while 6 cases showed less than 50 % decrease in AHI. There was significant " +
            "positive correlation between changes of AHI and changes of neck circumference while no " +
            "significant correlation between changes of AHI and changes of BMI during the period of upper airway exercises.";
    private TextView taDesp;
    private Button btnAbout;
    private Button btnHow;
    private Button btnStrong;
    private Button btnMedium;
    private Button btnWeak;
    private Button btnHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //screen configuration
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_help);

        //set up ui
        taDesp = findViewById(R.id.taDesp);
        btnHow = findViewById(R.id.btnHow);
        btnAbout = findViewById(R.id.btnAbout);
        btnStrong = findViewById(R.id.btnStrong);
        btnMedium = findViewById(R.id.btnMedium);
        btnWeak = findViewById(R.id.btnWeak);
        btnHome = findViewById(R.id.btnHome);
        taDesp.setText(howToPlay);

        btnHow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taDesp.setText(howToPlay);
            }
        });

        btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taDesp.setText(aboutApp);
            }
        });

        btnStrong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taDesp.setText("You need to move your tongue with all your strength while playing the game!");
            }
        });
        btnMedium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taDesp.setText("You need to move your tongue with moderate strength while playing the game.");
            }
        });
        btnWeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taDesp.setText("A little strength is required when you are moving your tongue. However, it is very sensitive to any other movements around you.");
            }
        });

        //return to home page

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
