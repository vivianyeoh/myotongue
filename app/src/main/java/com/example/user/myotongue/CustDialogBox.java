package com.example.user.myotongue;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * A custom alert dialog box that accept title and message input and display them in dialog nox
 */

public class CustDialogBox extends AlertDialog {
    String message;
    Context context;
    AlertDialog.Builder builder;

    protected CustDialogBox(Context context, String title,String message) {
        super(context);
        this.message = message;
        this.context = context;
        builder = new AlertDialog.Builder(context);

        //show message
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                })
                .show();
    }
}
