package com.example.user.myotongue;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * SQLiteOpenHelper is used to manage user data. Every user session will be stored and display by date.
 */
public class DBHelper extends SQLiteOpenHelper {
    //constants for database tables
    public static final String COLUMN_TID = "tid";
    public static final String COLUMN_LEVEL = "level";
    public static final String COLUMN_LEFTPOSITION = "leftPosition";
    public static final String COLUMN_RIGHTPOSITION = "rightPosition";
    public static final String COLUMN_UPPOSITION = "upPosition";
    public static final String COLUMN_DOWNPOSITION = "downPosition";
    public static final String COLUMN_STARTDATE = "startdate";
    public static final String COLUMN_STAGE = "stage";
    static final String TABLE_TONGUE = "TongueDB";
    static final String DATABASE_NAME = "tongueDB.db";
    static final int DATABASE_VERSION = 1;
    private static final String TAG = "DBHelper";
    public final String CREATE_TABLE = "CREATE TABLE " + TABLE_TONGUE + "("
            + COLUMN_TID + " INTEGER PRIMARY KEY Autoincrement,"
            + COLUMN_LEVEL + " INTEGER,"
            + COLUMN_LEFTPOSITION + " INTEGER, "
            + COLUMN_RIGHTPOSITION + " INTEGER, "
            + COLUMN_UPPOSITION + " INTEGER, "
            + COLUMN_DOWNPOSITION + " INTEGER, "
            + COLUMN_STARTDATE + " DATETIME, "
            + COLUMN_STAGE + " INTEGER"
            + ")";
    private String[] projection = {COLUMN_TID, COLUMN_LEVEL, COLUMN_LEFTPOSITION, COLUMN_RIGHTPOSITION, COLUMN_UPPOSITION, COLUMN_DOWNPOSITION, COLUMN_STARTDATE,COLUMN_STAGE};

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //create table
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //when migrating from one database to another, drop table if exist and create a new one.
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TONGUE);
        onCreate(db);
    }

    /*add session into database*/
    public int addTongue(Tongue tongue) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_LEVEL, tongue.getLevel());
        values.put(COLUMN_LEFTPOSITION, tongue.getLeftPosition());
        values.put(COLUMN_RIGHTPOSITION, tongue.getRightPosition());
        values.put(COLUMN_UPPOSITION, tongue.getUpPosition());
        values.put(COLUMN_DOWNPOSITION, tongue.getDownPosition());
        values.put(COLUMN_STARTDATE, tongue.getStartDate());
        values.put(COLUMN_STAGE, tongue.getStage());
        return (int) db.insert(TABLE_TONGUE, null, values);
    }

    /*return an arraylist of data between two date*/
    public ArrayList<Tongue> getResult(String fro, String to) {
        //query data from sqlite
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<Tongue> array_list = new ArrayList<>();
        Cursor cursor = db.query(TABLE_TONGUE, projection,  COLUMN_STARTDATE +" between '"+fro+"' and '"+to+"'", null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                array_list.add(new Tongue(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getInt(5), cursor.getString(6), cursor.getInt(7)));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return array_list;
    }


    //this part code will be implemented in future
//    public Tongue getTongue(int tongueid) {
//        SQLiteDatabase db = getReadableDatabase();
//        Cursor cursor = db.query(TABLE_TONGUE, projection, COLUMN_TID, new String[]{tongueid + ""}, null, null, null);
//        Tongue tongue = null;
//        if (cursor.moveToFirst()) {
//            cursor.moveToFirst();
//            tongue = new Tongue(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getInt(5), cursor.getString(6), cursor.getInt(7));
//            cursor.close();
//        }
//        Log.e(TAG, tongue.getTid() + "m is gotten from content provider!");
//        return tongue;
//    }
//
//    public void updateTongue(Tongue tongue) {
//        SQLiteDatabase db = getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(COLUMN_LEVEL, tongue.getLevel());
//        values.put(COLUMN_LEFTPOSITION, tongue.getLeftPosition());
//        values.put(COLUMN_RIGHTPOSITION, tongue.getRightPosition());
//        values.put(COLUMN_UPPOSITION, tongue.getUpPosition());
//        values.put(COLUMN_DOWNPOSITION, tongue.getDownPosition());
//        values.put(COLUMN_STARTDATE, tongue.getStartDate());
//        values.put(COLUMN_STAGE, tongue.getStage());
//        db.update(TABLE_TONGUE, values, COLUMN_TID, new String[]{tongue.getTid() + ""});
//        Log.e(TAG, tongue.getTid() + "m is updated in content provider!");
//
//    }
//
//    public void deleteTongue(int tongueid) {
//        SQLiteDatabase db = getWritableDatabase();
//        db.delete(TABLE_TONGUE, COLUMN_TID, new String[]{tongueid + ""});
//        Log.e(TAG, "Tongue with id: " + tongueid + " is deleted from content provider!");
//    }
//
//    public ArrayList<Tongue> getAllTongues() {
//        //get all tongues from content provider and return it as an array list
//        SQLiteDatabase db = getReadableDatabase();
//        ArrayList<Tongue> array_list = new ArrayList<>();
//        Cursor cursor = db.query(TABLE_TONGUE, projection, null, null, null, null, null);
//        if (cursor != null) {
//            cursor.moveToFirst();
//            for (int i = 0; i < cursor.getCount(); i++) {
//                array_list.add(new Tongue(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getInt(5), cursor.getString(6), cursor.getInt(7)));
//                Log.e(TAG,array_list.get(i).toString());
//                cursor.moveToNext();
//            }
//            cursor.close();
//        }
//        return array_list;
//    }
}
