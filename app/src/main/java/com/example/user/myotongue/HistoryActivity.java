package com.example.user.myotongue;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**/

public class HistoryActivity extends AppCompatActivity {
    private static final String TAG = "HistoryActivity";
    //display variables
    private final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    //ui variables
    private ImageButton btnInfo;
    private ImageButton btnHome;
    private TextView tvSUp;
    private TextView tvSDown;
    private TextView tvSLeft;
    private TextView tvSRight;
    private TextView tvMUp;
    private TextView tvMDown;
    private TextView tvMLeft;
    private TextView tvMRight;
    private TextView tvWUp;
    private TextView tvWDown;
    private TextView tvWLeft;
    private TextView tvWRight;
    private TextView tvDateTo;
    private TextView tvDateFro;
    private TextView tvSSession;
    private TextView tvMSession;
    private TextView tvWSession;
    private String dayTo;
    private String dayFro;
    private DBHelper db;
    private ImageButton btnShow;
    private TextView tvWStage;
    private TextView tvSStage;
    private TextView tvMStage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //configure screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_history);

        //set up ui
        btnShow = findViewById(R.id.btnShow);
        btnInfo = findViewById(R.id.btnInfo);
        btnHome = findViewById(R.id.btnHome);

        tvSUp = findViewById(R.id.tvSUp);
        tvSDown = findViewById(R.id.tvSDown);
        tvSLeft = findViewById(R.id.tvSLeft);
        tvSRight = findViewById(R.id.tvSRight);
        tvSSession = findViewById(R.id.tvSSession);
        tvSStage = findViewById(R.id.tvSStage);

        tvMUp = findViewById(R.id.tvMUp);
        tvMDown = findViewById(R.id.tvMDown);
        tvMLeft = findViewById(R.id.tvMLeft);
        tvMRight = findViewById(R.id.tvMRight);
        tvMSession = findViewById(R.id.tvMSession);
        tvMStage = findViewById(R.id.tvMStage);

        tvWUp = findViewById(R.id.tvWUp);
        tvWDown = findViewById(R.id.tvWDown);
        tvWLeft = findViewById(R.id.tvWLeft);
        tvWRight = findViewById(R.id.tvWRight);
        tvWSession = findViewById(R.id.tvWSession);
        tvWStage = findViewById(R.id.tvWStage);

        tvDateTo = findViewById(R.id.tvDateTo);
        tvDateFro = findViewById(R.id.tvDateFro);

        //set up ui and database
        configureUI();
        db = new DBHelper(HistoryActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setResult(dayFro, dayTo);
    }

    //get data from sqlite and set text with data from date picker
    public void setResult(String fro, String to) {
        if (fro.compareTo(to) <= 0) {
            ArrayList<Tongue> tongueList = db.getResult(fro, to);
            int leftS, rightS, upS, downS, noOfSessionS, stageS;
            int leftM, rightM, upM, downM, noOfSessionM, stageM;
            int leftW, rightW, upW, downW, noOfSessionW, stageW;
            leftS = leftM = leftW = rightS = rightM = rightW = upS = upM = upW = downS = downM = downW = noOfSessionS = noOfSessionM = noOfSessionW = stageS = stageM = stageW = 0;
            //compute frequency
            if (tongueList != null) {
                for (Tongue t : tongueList) {
                    if (t.getLevel() == 0) {//0 is strong
                        leftS += t.getLeftPosition();
                        rightS += t.getRightPosition();
                        upS += t.getUpPosition();
                        downS += t.getDownPosition();
                        noOfSessionS++;
                        //find maximum number of stage
                        if (t.getStage() > stageS)
                            stageS = t.getStage();
                    } else if (t.getLevel() == 1) {//1 is medium
                        leftM += t.getLeftPosition();
                        rightM += t.getRightPosition();
                        upM += t.getUpPosition();
                        downM += t.getDownPosition();
                        noOfSessionM++;
                        //find maximum number of stage
                        if (t.getStage() > stageM)
                            stageM = t.getStage();
                    } else if (t.getLevel() == 2) {//2 is weak
                        leftW += t.getLeftPosition();
                        rightW += t.getRightPosition();
                        upW += t.getUpPosition();
                        noOfSessionW++;
                        downW += t.getDownPosition();
                        //find maximum number of stage
                        if (t.getStage() > stageW)
                            stageW = t.getStage();
                    }
                }
            }
            tvSUp.setText(upS + "");
            tvSDown.setText(downS + "");
            tvSLeft.setText(leftS + "");
            tvSRight.setText(rightS + "");
            tvSSession.setText(noOfSessionS + "");
            tvSStage.setText(stageS + "");

            tvMUp.setText(upM + "");
            tvMDown.setText(downM + "");
            tvMLeft.setText(leftM + "");
            tvMRight.setText(rightM + "");
            tvMSession.setText(noOfSessionM + "");
            tvMStage.setText(stageM + "");

            tvWUp.setText(upW + "");
            tvWDown.setText(downW + "");
            tvWLeft.setText(leftW + "");
            tvWRight.setText(rightW + "");
            tvWSession.setText(noOfSessionW + "");
            tvWStage.setText(stageW + "");
        }
    }

    public void configureUI() {
        dayFro = dateFormatter.format(new Date());
        dayTo = dateFormatter.format(new Date());
        tvDateFro.setText(dateFormatter.format(new Date()));
        tvDateTo.setText(dateFormatter.format(new Date()));

        tvDateTo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //show date picker
                Calendar calendar = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(HistoryActivity.this, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        dayTo = dateFormatter.format(newDate.getTime());
                        tvDateTo.setText(dayTo);
                        setResult(dayFro, dayTo);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();

            }
        });

        tvDateFro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show date picker
                Calendar calendar = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(HistoryActivity.this, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        dayFro = dateFormatter.format(newDate.getTime());
                        tvDateFro.setText(dayFro);
                        setResult(dayFro, dayTo);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();

            }
        });

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CustDialogBox(HistoryActivity.this, "Info", "Click on the dates to choose the time period and press the button right next to date to display the results.");
            }
        });

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // display data between 2 dates return from database
        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Date fro = dateFormatter.parse(dayFro);
                    Date to = dateFormatter.parse(dayTo);
                    if (fro.compareTo(to) > 0) {
                        new CustDialogBox(HistoryActivity.this, "Info", "Second date must be later than first one");
                    } else {
                        setResult(dateFormatter.format(fro), dateFormatter.format(to));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
