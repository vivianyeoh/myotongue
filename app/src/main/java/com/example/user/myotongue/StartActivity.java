package com.example.user.myotongue;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;
import org.opencv.video.Video;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
* This activity is the main interface for Tongue movement tracker. User will position their face in
* the blue box shown on camera until an orange box appears around their face. Then they should press start.
* Each stage requires 5 ticks for each posture. Once they have completed 5 ticks for every posture,
* they should proceed to the next stage.
* The position is detected by calculate the tangent angle between 2 points. Some constraints were exerted.
* It will be explained more in the report.
* */

public class StartActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {
    public static final String MyPREFERENCES = "MyotonguePref";
    private static final String TAG = "StartActivity";
    private final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    private Tongue currentSession;
    private DBHelper db;
    private boolean isPause = false, isStarted = false, buttonIsEnabled = false;
    private int prevPostureCount = 0, stage = 1;
    private int scoreLeft, scoreRight, scoreUp, scoreDown;
    private int progressLeft, progressRight, progressUp, progressDown, progress;
    private String prevPosture = "";

    //user interface
    private LinearLayout viewStart, viewPause, viewScore, viewStartCamera;
    private ImageButton btnInfo, btnPause;
    private ProgressBar progressBar;
    private Spinner spinner;
    private SharedPreferences sharedpreferences;
    private ImageView checkUp1, checkUp2, checkUp3, checkUp4, checkUp5, checkRight1, checkRight2, checkRight3, checkRight4, checkRight5;
    private ImageView checkDown1, checkDown2, checkDown3, checkDown4, checkDown5, checkLeft1, checkLeft2, checkLeft3, checkLeft4, checkLeft5;
    private TextView tvUp, tvDown, tvLeft, tvRight, tvScore, tvTime, tvStageResult;
    private TextView tvStage, tvLevel, tvShowLevel, tvWarning, tvWarning2;
    private TextView tvInstrUp, tvInstrDown, tvInstrLeft, tvInstrRight, tvProgress;
    private Button btnStart, btnHome, btnHome2, btnHome3;
    private Button btnAgain, btnBack, btnStartGame, btnShowGrid, btnTutorial;

    //optical flow
    private MatOfPoint features;
    private MatOfPoint2f prevFeatures, nextFeatures;
    private MatOfByte status;
    private MatOfFloat err;
    private Mat mRgba, mGray, mPrevGray;
    private CameraBridgeViewBase mOpenCvCameraView;
    private CascadeClassifier faceCascade, mouthCascade;
    private int absoluteMouthSize, absoluteFaceSize;
    private Point mouthStartPoint = new Point(), mouthEndPoint = new Point();
    private Point faceStartPoint = new Point(), faceEndPoint = new Point();
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        //OpenCV manager to help our app communicate with android phone to make OpenCV work
        //Once OpenCV library is loaded, you may want to perform some actions. For example,
        // displaying a success or failure message.
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    try {
                        // load mouth cascade file from application resources
                        InputStream is = getResources().openRawResource(R.raw.haarcascade_mcs_mouth);
                        File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                        File mCascadeMouthFile = new File(cascadeDir, "haarcascade_mcs_mouth.xml");
                        FileOutputStream os = new FileOutputStream(mCascadeMouthFile);

                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();
                        mouthCascade = new CascadeClassifier();
                        mouthCascade.load(mCascadeMouthFile.getAbsolutePath());
                        cascadeDir.delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load mouth cascade. Exception thrown: " + e);
                    }
                    try {
                        // load face cascade file from application resources
                        InputStream isFace = getResources().openRawResource(R.raw.lbpcascade_frontalface);
                        File cascadeDirFace = getDir("cascade", Context.MODE_PRIVATE);
                        File mCascadeFaceFile = new File(cascadeDirFace, "lbpcascade_frontalface.xml");
                        FileOutputStream osFace = new FileOutputStream(mCascadeFaceFile);

                        byte[] bufferFace = new byte[4096];
                        int bytesReadFace;
                        while ((bytesReadFace = isFace.read(bufferFace)) != -1) {
                            osFace.write(bufferFace, 0, bytesReadFace);
                        }
                        isFace.close();
                        osFace.close();
                        faceCascade = new CascadeClassifier();
                        faceCascade.load(mCascadeFaceFile.getAbsolutePath());
                        cascadeDirFace.delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load face cascade. Exception thrown: " + e);
                    }
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //screen configuration
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_start);

        //set up ui and database
        initializeInterface();
        db = new DBHelper(StartActivity.this);
        currentSession = null;

    }

    /*initialize all ui elements*/
    private void initializeInterface() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        scoreLeft = scoreRight = scoreUp = scoreDown = 0;

        progressBar = findViewById(R.id.pbUp);
        progressBar.setScaleY(6f);
        progressBar.setMax(20);
        progressBar.getProgressDrawable().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);

        viewStart = findViewById(R.id.viewStart);
        viewPause = findViewById(R.id.viewPause);
        viewScore = findViewById(R.id.viewScore);
        viewStartCamera = findViewById(R.id.viewStartCamera);
        viewStart.setVisibility(View.VISIBLE);

        spinner = findViewById(R.id.spinner);
        setSpinner();

        tvLevel = findViewById(R.id.tvLevel);
        tvUp = findViewById(R.id.tvUp);
        tvDown = findViewById(R.id.tvDown);
        tvLeft = findViewById(R.id.tvLeft);
        tvRight = findViewById(R.id.tvRight);
        tvInstrUp = findViewById(R.id.tvInstrUp);
        tvInstrDown = findViewById(R.id.tvInstrDown);
        tvInstrLeft = findViewById(R.id.tvInstrLeft);
        tvInstrRight = findViewById(R.id.tvInstrRight);
        tvProgress = findViewById(R.id.tvProgress);
        tvScore = findViewById(R.id.tvScore);
        tvTime = findViewById(R.id.tvTime);
        tvStage = findViewById(R.id.tvStage);
        tvWarning = findViewById(R.id.tvWarning);
        tvWarning2 = findViewById(R.id.tvWarning2);
        tvStageResult = findViewById(R.id.tvStageResult);
        tvShowLevel = findViewById(R.id.tvShowLevel);

        checkUp1 = findViewById(R.id.checkUp1);
        checkUp2 = findViewById(R.id.checkUp2);
        checkUp3 = findViewById(R.id.checkUp3);
        checkUp4 = findViewById(R.id.checkUp4);
        checkUp5 = findViewById(R.id.checkUp5);
        checkRight1 = findViewById(R.id.checkRight1);
        checkRight2 = findViewById(R.id.checkRight2);
        checkRight3 = findViewById(R.id.checkRight3);
        checkRight4 = findViewById(R.id.checkRight4);
        checkRight5 = findViewById(R.id.checkRight5);
        checkDown1 = findViewById(R.id.checkDown1);
        checkDown2 = findViewById(R.id.checkDown2);
        checkDown3 = findViewById(R.id.checkDown3);
        checkDown4 = findViewById(R.id.checkDown4);
        checkDown5 = findViewById(R.id.checkDown5);
        checkLeft1 = findViewById(R.id.checkLeft1);
        checkLeft2 = findViewById(R.id.checkLeft2);
        checkLeft3 = findViewById(R.id.checkLeft3);
        checkLeft4 = findViewById(R.id.checkLeft4);
        checkLeft5 = findViewById(R.id.checkLeft5);

        btnInfo = findViewById(R.id.btnInfo);
        btnPause = findViewById(R.id.btnPause);
        btnStartGame = findViewById(R.id.btnStartGame);
        btnBack = findViewById(R.id.btnBack);
        btnStart = findViewById(R.id.btnStart);
        btnHome = findViewById(R.id.btnHome);
        btnHome2 = findViewById(R.id.btnHome2);
        btnHome3 = findViewById(R.id.btnHome3);
        btnAgain = findViewById(R.id.btnAgain);
        btnShowGrid = findViewById(R.id.btnShowGrid);
        btnTutorial = findViewById(R.id.btnTutorial);
        setButtons();

        mOpenCvCameraView = (JavaCameraView) findViewById(R.id.show_camera_activity_java_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(StartActivity.this);
    }

    /*set normal buttons function*/
    public void setButtons() {

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (buttonIsEnabled) {
                    if (isStarted) {
                        isPause = true;
                        viewPause.setVisibility(View.VISIBLE);
                        //btnInfo is not visible so it should not be pressed by user
                        buttonIsEnabled = false;
                    }
                    new CustDialogBox(StartActivity.this, "Info", HelpActivity.howToPlay);
                }
            }
        });

        btnTutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CustDialogBox(StartActivity.this, "Info", HelpActivity.howToPlay);
            }
        });

        //set button text
        int showGrid = sharedpreferences.getInt("showGrid", 0);//0 is no
        if (showGrid == 0) {
            btnShowGrid.setText("Show Grid");
        } else {
            btnShowGrid.setText("Hide Grid");
        }
        btnShowGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (buttonIsEnabled) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int showGrid = sharedpreferences.getInt("showGrid", 0);//0 is no
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            if (showGrid == 0) {
                                editor.putInt("showGrid", 1);
                                btnShowGrid.setText("Hide Grid");
                            } else {
                                editor.putInt("showGrid", 0);
                                btnShowGrid.setText("Show Grid");
                            }
                            editor.apply();
                        }
                    });
                }
            }
        });

        //start recognize user face
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewStart.setVisibility(View.GONE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putLong("timeRemaining", 30000);
                editor.apply();
                resetStartCamera();
                int level = sharedpreferences.getInt("level", 1);
                if (level == 0) tvShowLevel.setText("Level: Hard");
                else if (level == 1) tvShowLevel.setText("Level: Medium");
                else if (level == 2) tvShowLevel.setText("Level: Easy");
                //allow button on game interface to be pressed
                buttonIsEnabled = true;
            }
        });

        //button that start game after user face has been recognized and stored
        btnStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStarted = true;
                viewStartCamera.setVisibility(View.GONE);
                long time = sharedpreferences.getLong("timeRemaining", 30000);
                startSession(time);
                buttonIsEnabled = true;
            }
        });

        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (buttonIsEnabled) {
                    isPause = true;
                    viewPause.setVisibility(View.VISIBLE);
                }
                //btnPause is not visible so it should not be pressed by user
                buttonIsEnabled = false;
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPause = false;
                viewPause.setVisibility(View.GONE);
                resetStartCamera();
                buttonIsEnabled = true;
            }
        });

        //button that restart game after user has played the game for 30s
        btnAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewScore.setVisibility(View.GONE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putLong("timeRemaining", 30000);
                editor.apply();
                resetStartCamera();
                isPause = false;
                viewPause.setVisibility(View.GONE);
                buttonIsEnabled = true;
            }
        });

        //return to home page
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnHome2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnHome3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /*set up spinner*/
    public void setSpinner() {

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Hard");
        categories.add("Medium");
        categories.add("Easy");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        int spinnerPosition = sharedpreferences.getInt("level", 1);
        spinner.setSelection(spinnerPosition);
        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                //store level chosen by user into user preference
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt("level", position);
                editor.apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //auto generate
            }
        });
    }

    /*start game or return to game on pause*/
    public void startSession(long duration) {

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putLong("timeRemaining", duration);
        editor.apply();
        if (duration < 30000) {
            updateProcessIcon(false, progressDown, checkDown1, checkDown2, checkDown3, checkDown4, checkDown5);
            updateProcessIcon(false, progressUp, checkUp1, checkUp2, checkUp3, checkUp4, checkUp5);
            updateProcessIcon(false, progressRight, checkRight1, checkRight2, checkRight3, checkRight4, checkRight5);
            updateProcessIcon(false, progressLeft, checkLeft1, checkLeft2, checkLeft3, checkLeft4, checkLeft5);
        } else {
            updateProcessIcon(true, 0, checkDown1, checkDown2, checkDown3, checkDown4, checkDown5);
            updateProcessIcon(true, 0, checkUp1, checkUp2, checkUp3, checkUp4, checkUp5);
            updateProcessIcon(true, 0, checkRight1, checkRight2, checkRight3, checkRight4, checkRight5);
            updateProcessIcon(true, 0, checkLeft1, checkLeft2, checkLeft3, checkLeft4, checkLeft5);
        }
        new CountDownTimer(duration, 500) {
            @Override
            public void onTick(long millisUntilFinished) {

                Long timeRemaining = millisUntilFinished;
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putLong("timeRemaining", timeRemaining);
                editor.apply();

                //if the game is paused, cancel the countdown
                if (isPause) {
                    cancel();
                } else {
                    //if all postures has 5 checks each, enter the next stage
                    if (progressLeft == 5 && progressDown == 5 && progressRight == 5 && progressUp == 5) {
                        stage++;
                        updateProcessIcon(true, 0, checkDown1, checkDown2, checkDown3, checkDown4, checkDown5);
                        updateProcessIcon(true, 0, checkUp1, checkUp2, checkUp3, checkUp4, checkUp5);
                        updateProcessIcon(true, 0, checkRight1, checkRight2, checkRight3, checkRight4, checkRight5);
                        updateProcessIcon(true, 0, checkLeft1, checkLeft2, checkLeft3, checkLeft4, checkLeft5);
                        progressDown = 0;
                        progressUp = 0;
                        progressRight = 0;
                        progressLeft = 0;
                        progress = 0;
                    }
                    tvTime.setText(millisUntilFinished / 1000 + "s");
                    tvUp.setText(scoreUp + "");
                    tvLeft.setText(scoreLeft + "");
                    tvDown.setText(scoreDown + "");
                    tvRight.setText(scoreRight + "");

                    tvInstrUp.setText((5 - progressUp) + " needed");
                    tvInstrLeft.setText((5 - progressLeft) + " needed");
                    tvInstrDown.setText((5 - progressDown) + " needed");
                    tvInstrRight.setText((5 - progressRight) + " needed");
                    tvStage.setText("Stage " + stage);
                    progressBar.setProgress(progress);
                    tvProgress.setText((int) ((double) progress / 20 * 100) + "%");
                }
            }

            @Override
            public void onFinish() {
                //stop game
                isPause = false;
                isStarted = false;
                int level = sharedpreferences.getInt("level", 1);
                if (level == 0) tvLevel.setText("Hard");
                else if (level == 1) tvLevel.setText("Medium");
                else if (level == 2) tvLevel.setText("Easy");
                currentSession = new Tongue(level, scoreLeft, scoreRight, scoreUp, scoreDown, dateFormatter.format(new Date()), stage);
                db.addTongue(currentSession);
                viewScore.setVisibility(View.VISIBLE);
                tvStageResult.setText("Stage " + stage);
                tvScore.setText("Up: " + scoreUp + ", Down: " + scoreDown + ", Left: " + scoreLeft + ", Right: " + scoreRight);
                currentSession = null;

                //prepare for if game is restarted
                scoreLeft = scoreRight = scoreUp = scoreDown = 0;
                progressLeft = progressRight = progressUp = progressDown = 0;
                stage = 1;
                progress = 0;
                tvTime.setText("00s");
                tvLeft.setText(scoreLeft + "");
                tvRight.setText(scoreRight + "");
                tvUp.setText(scoreUp + "");
                tvDown.setText(scoreDown + "");

                tvInstrUp.setText((5 - progressUp) + " needed");
                tvInstrLeft.setText((5 - progressLeft) + " needed");
                tvInstrDown.setText((5 - progressDown) + " needed");
                tvInstrRight.setText((5 - progressRight) + " needed");
                tvStage.setText("Stage: " + stage);
                progressBar.setProgress(0);
                tvProgress.setText("0%");

                updateProcessIcon(true, 0, checkUp1, checkUp2, checkUp3, checkUp4, checkUp5);
                updateProcessIcon(true, 0, checkLeft1, checkLeft2, checkLeft3, checkLeft4, checkLeft5);
                updateProcessIcon(true, 0, checkRight1, checkRight2, checkRight3, checkRight4, checkRight5);
                updateProcessIcon(true, 0, checkDown1, checkDown2, checkDown3, checkDown4, checkDown5);
                //buttons are not visible so it should not be pressed by user
                buttonIsEnabled = false;
            }
        }.start();
    }

    /*reset the value in points*/
    private void resetStartCamera() {
        mouthStartPoint = new Point();
        mouthEndPoint = new Point();
        faceStartPoint = new Point();
        faceEndPoint = new Point();
        viewStartCamera.setVisibility(View.VISIBLE);
        isStarted = false;
        btnStartGame.setEnabled(false);
        btnStartGame.setText("Put your face in that box!");
    }

    /*android lifecycle on resume*/
    @Override
    public void onResume() {
        super.onResume();
        //load opencv into the project
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, StartActivity.this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
        //if view was paused, show view pause layout
        if (isPause) {
            viewPause.setVisibility(View.VISIBLE);
        }
    }

    /*android lifecycle on pause*/
    @Override
    public void onPause() {
        super.onPause();
        //show view pause layout
        if (!isPause && isStarted) {
            isPause = true;
            isStarted = false;
            //buttons are not visible so it should not be pressed by user
            buttonIsEnabled = false;
            viewPause.setVisibility(View.VISIBLE);
        }
        //disable camera view
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    /*android lifecycle on destroy*/
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    /* function from CameraBridgeViewBase.CvCameraViewListener2
    * set up camera variables*/
    @Override
    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mGray = new Mat(height, width, CvType.CV_8UC1);
        resetVars();
        //face detection - compute minimum face size
        if (this.absoluteFaceSize == 0) {
            int h = mGray.rows();
            if (Math.round(h * 0.3f) > 0) {
                this.absoluteFaceSize = Math.round(h * 0.3f);
            }
        }
        //mouth detection - compute minimum mouth size (20% of the frame height, in our case)
        if (this.absoluteMouthSize == 0) {
            int h = absoluteFaceSize;
            if (Math.round(h * 0.4f) > 0) {
                this.absoluteMouthSize = Math.round(h * 0.4f);
            }
        }
    }

    /* function from CameraBridgeViewBase.CvCameraViewListener2
    * release data*/
    @Override
    public void onCameraViewStopped() {
        mRgba.release();
        mGray.release();
    }

    /* function from CameraBridgeViewBase.CvCameraViewListener2
    * things to do when camera is enabled*/
    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();

        //make mirror view
        Core.flip(mRgba, mRgba, 1);
        Core.flip(mGray, mGray, 1);
        //set fixed face rectangle that the user rectangle must stay inside this rectangle
        Point fixedFaceStart = new Point(mOpenCvCameraView.getWidth() / 6, mOpenCvCameraView.getHeight() / 10);
        Point fixedFaceEnd = new Point(mOpenCvCameraView.getWidth() / 6 * 5, mOpenCvCameraView.getHeight() / 10 * 8);
        Imgproc.rectangle(mRgba, fixedFaceStart, fixedFaceEnd, new Scalar(0, 0, 255), 6);
        //if the user face is not detected, detect user face
        if (!isStarted) {
            // detect face with opencv function
            MatOfRect faces = new MatOfRect();
            this.faceCascade.detectMultiScale(mGray, faces, 1.1, 2, Objdetect.CASCADE_SCALE_IMAGE,
                    new Size(this.absoluteFaceSize, this.absoluteFaceSize), new Size());

            final Rect[] faceArray = faces.toArray();
            if (faceArray.length > 0) {
                //use the first value in the array because it is more likely to be a correct one
                final double faceStartx = faceArray[0].tl().x;
                final double faceStarty = faceArray[0].tl().y;
                final double faceEndx = faceArray[0].br().x;
                final double faceEndy = faceArray[0].br().y;
                double faceCenterx = (faceStartx + faceEndx) / 2;
                double faceCentery = (faceStarty + faceEndy) / 2;
                // detect mouths
                MatOfRect mouths = new MatOfRect();
                this.mouthCascade.detectMultiScale(mGray, mouths, 1.1, 2, Objdetect.CASCADE_SCALE_IMAGE,
                        new Size(this.absoluteMouthSize, this.absoluteMouthSize), new Size());

                final Rect[] mouthsArray = mouths.toArray();
                if (mouthsArray.length > 0) {
                    final double mouthStartx = mouthsArray[0].tl().x;
                    final double mouthStarty = mouthsArray[0].tl().y;
                    final double mouthEndx = mouthsArray[0].br().x;
                    final double mouthEndy = mouthsArray[0].br().y;
                    //position of mouth must be at the lower half of their face
                    if (mouthStartx > faceStartx
                            && mouthStartx < faceCenterx
                            && mouthStarty > faceCentery
                            && faceEndx > mouthEndx
                            && faceEndy > mouthEndy) {
                        //calculate distance between user face bounding box and fixed face bounding box
                        //and add constraint
                        //user's face must be inside the box or not too far from the bounding box
                        if (euclideanDist(fixedFaceStart, faceArray[0].tl()) < 10
                                | euclideanDist(fixedFaceEnd, faceArray[0].br()) < 10
                                | (faceStartx > fixedFaceStart.x
                                && faceStarty > fixedFaceStart.y
                                && fixedFaceEnd.x > faceEndx
                                && fixedFaceEnd.y > faceEndy)) {
                            //if the above conditions are met, set user face point
                            mouthStartPoint = mouthsArray[0].tl();
                            mouthEndPoint = mouthsArray[0].br();
                            faceStartPoint = faceArray[0].tl();
                            faceEndPoint = faceArray[0].br();
                            //enables start button to start the game
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    btnStartGame.setEnabled(true);
                                    btnStartGame.setText("Start");
                                }
                            });
                            //draw user face bounding box
                            Imgproc.rectangle(mRgba, faceArray[0].tl(), faceArray[0].br(), new Scalar(232, 114, 11), 3);
                        } else {
                            //if user face did not meet condition, warn user about it in start button
                            mouthStartPoint = new Point();
                            mouthEndPoint = new Point();
                            faceStartPoint = new Point();
                            faceEndPoint = new Point();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    btnStartGame.setEnabled(false);
                                    btnStartGame.setText("Put your face in that box!");
                                }
                            });
                        }
                    }
                }
            } else {
                mouthStartPoint = new Point();
                mouthEndPoint = new Point();
                faceStartPoint = new Point();
                faceEndPoint = new Point();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnStartGame.setEnabled(false);
                        btnStartGame.setText("Put your face in that box!");
                    }
                });
            }
        }

        //run optical flow around mouth area with reserved face and mouth area
        if (features.toArray().length == 0) {
            //set up feature points for the whole camera
            int rowStep = 12, colStep = 25;
            int nRows = mGray.rows() / rowStep, nCols = mGray.cols() / colStep;
            Point points[] = new Point[nRows * nCols];
            for (int i = 0; i < nRows; i++) {
                for (int j = 0; j < nCols; j++) {
                    points[i * nCols + j] = new Point(j * colStep, i * rowStep);
                }
            }
            features.fromArray(points);
            prevFeatures.fromList(features.toList());
            mPrevGray = mGray.clone();
        }
        nextFeatures.fromArray(prevFeatures.toArray());
        //draw fixed rectangle of user bounding box
        Imgproc.rectangle(mRgba, faceStartPoint, faceEndPoint, new Scalar(232, 114, 11), 3);
        //calculate optical flow and store that data in nextFeatures
        Video.calcOpticalFlowPyrLK(mPrevGray, mGray, prevFeatures, nextFeatures, status, err);
        mPrevGray = mGray.clone();

        List<Point> prevList = features.toList();
        List<Point> nextList = nextFeatures.toList();
        Scalar color = new Scalar(255, 0, 0);

        int direction[] = new int[4];
        int movedPoint = 0, totalPoint = 0;
        for (int i = 0; i < prevList.size(); i++) {
            double x1 = prevList.get(i).x;
            double y1 = prevList.get(i).y;
            double x2 = nextList.get(i).x;
            double y2 = nextList.get(i).y;
            //returned data must stay inside fixed mouth bounding box
            if (x1 > 0 && y1 > 0 && x2 > 0 && y2 > 0)
                if (x1 > mouthStartPoint.x && y1 > mouthStartPoint.y)
                    if (x2 > mouthStartPoint.x && y2 > mouthStartPoint.y)
                        if (x1 < mouthEndPoint.x && y1 < mouthEndPoint.y)
                            if (x2 < mouthEndPoint.x && y2 < mouthEndPoint.y) {
                                totalPoint++;
                                //to filter noise, the distance between prev point and next point must more than 5
                                if (euclideanDist(prevList.get(i), nextList.get(i)) > 3) {
                                    //calculate number of points that have met the condition
                                    movedPoint++;
                                    if (euclideanDist(prevList.get(i), nextList.get(i)) > 5) {
                                        //set result with the function below
                                        direction = findDirection(prevList.get(i), nextList.get(i), direction);
                                    }
                                }
                                //draw the prev and next points
                                int showGrid = sharedpreferences.getInt("showGrid", 0);//0 is no
                                if (showGrid == 1)
                                    Imgproc.line(mRgba, prevList.get(i), nextList.get(i), color, 5);
                                else
                                    Imgproc.rectangle(mRgba, mouthStartPoint, mouthEndPoint, color, 3);
                            }
        }
        //store final direction in dir
        final String dir = displayDir(direction, sharedpreferences.getInt("level", 1), movedPoint, totalPoint);
        //display result in the camera screen
//        Imgproc.putText(mRgba, "up:" + direction[0] + " right:" + direction[1] + " down:" + direction[2] + " left:" + direction[3],
//                new Point(0, 100), Core.FONT_HERSHEY_SIMPLEX,// front face
//                1, new Scalar(255, 0, 0), 2);
        //if game has been started and it is not paused
        if (!isPause && isStarted) {
            //user tongue will go to one direction in a few frame, therefore, noise will be created.
            //to filter noise, set timer to eliminate same direction data
            //next position cannot same as previous one in a small time period
            if (!dir.equals(prevPosture)) {
                switch (dir) {
                    //switch the position
                    //for that case, set text view and prevPosture with the result
                    //pause the camera view to let user see the actual position clearly
                    case "left":
                        if (progressLeft < 5) {
                            progress++;
                            scoreLeft++;
                            progressLeft++;
                            updateProcessIcon(false, progressLeft, checkLeft1, checkLeft2, checkLeft3, checkLeft4, checkLeft5);
                        }
                        prevPosture = "left";
                        break;
                    case "right":
                        if (progressRight < 5) {
                            progress++;
                            scoreRight++;
                            progressRight++;
                            updateProcessIcon(false, progressRight, checkRight1, checkRight2, checkRight3, checkRight4, checkRight5);
                        }
                        prevPosture = "right";
                        break;
                    case "up":
                        if (progressUp < 5) {
                            progress++;
                            scoreUp++;
                            progressUp++;
                            updateProcessIcon(false, progressUp, checkUp1, checkUp2, checkUp3, checkUp4, checkUp5);
                        }
                        prevPosture = "up";
                        break;
                    case "down":
                        if (progressDown < 5) {
                            progress++;
                            scoreDown++;
                            progressDown++;
                            updateProcessIcon(false, progressDown, checkDown1, checkDown2, checkDown3, checkDown4, checkDown5);
                        }
                        prevPosture = "down";
                        break;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (dir.equals("Move your tongue only!") | dir.equals(" ")) {
                            tvWarning.setText("Movement: none");
                            tvWarning2.setText(dir);
                        } else {
                            tvWarning.setText("Movement: " + dir);
                            tvWarning2.setText("Good Job");

                        }
                    }
                });
            } else {
                prevPostureCount++;
                if (prevPostureCount > 5) {
                    prevPosture = "";
                    prevPostureCount = 0;
                }
            }
        }
        return mRgba;
    }

    /* to calculate euclidean distance of two point*/
    private double euclideanDist(Point p, Point q) {
        double ycoord = Math.abs(p.y - q.y);
        double xcoord = Math.abs(p.x - q.x);
        return Math.sqrt((ycoord) * (ycoord) + (xcoord) * (xcoord));
    }

    /*return the position that most points in ttlPoint going to
    /*the maximum value should more than a threshold to reduce noise
    /*data is returned in a string datatype */
    private String displayDir(int[] dir, int level, int movedPoint, int totalPoint) {
        if (movedPoint > totalPoint * 90 / 100) {
            //if most point in movedPoint going toward same direction,
            //it might not be user's tongue moving
            return "Move your tongue only!";
        }

        int max = -1, index = 0, threshold;
        for (int i = 0; i < dir.length; i++) {
            if (dir[i] > max) {
                max = dir[i];
                index = i;
            }
        }
        //calculate threshold
        if (level == 0) {
            threshold = (int) ((double) movedPoint * 60 / 100);
        } else if (level == 1) {
            threshold = (int) ((double) movedPoint * 50 / 100);
        } else {
            threshold = (int) ((double) movedPoint * 40 / 100);
        }

        //compute result
        if (max > threshold && movedPoint >= 3) {
            if (index == 0)
                return "up";
            else if (index == 1)
                return "right";
            else if (index == 2)
                return "down";
            else if (index == 3)
                return "left";
            else
                return "error";
        } else {
            return " ";
        }
    }

    /* angle between two points is calculated with atan function
    * the angle will be used to find direction from one point to another
    * the result is stored in dir array
    * code referred from https://www.mathopenref.com/triggraphtan.html*/
    private int[] findDirection(Point prev, Point next, int[] dir) {
        double diffx = next.x - prev.x;
        double diffy = next.y - prev.y;
        double angle = Math.toDegrees(Math.atan(diffy / diffx));
        if (diffx > 0) {
            if (angle > 45) dir[2]++;//return "down";
            else if (angle > -45) dir[1]++;//return "right";
            else dir[0]++;//return "up";
        } else {
            angle += 180;
            if (angle < 135) dir[2]++;//return "down";
            else if (angle < 225) dir[3]++;//return "left";
            else dir[0]++;//return "up";
        }
        return dir;
    }

    /*reset mat variables*/
    private void resetVars() {
        mPrevGray = new Mat(mGray.rows(), mGray.cols(), CvType.CV_8UC1);
        features = new MatOfPoint();
        prevFeatures = new MatOfPoint2f();
        nextFeatures = new MatOfPoint2f();
        status = new MatOfByte();
        err = new MatOfFloat();
    }

    /* update checkbox with the number of p it should have*/
    private void updateProcessIcon(final boolean clear, final int p, final ImageView i1,
                                   final ImageView i2, final ImageView i3, final ImageView i4, final ImageView i5) {
        final ColorFilter redFilter = new PorterDuffColorFilter(Color.rgb(255, 0, 0), PorterDuff.Mode.SRC_ATOP);
        final ColorFilter blueFilter = new PorterDuffColorFilter(Color.rgb(0, 0, 255), PorterDuff.Mode.SRC_ATOP);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (clear) {
                    i1.setImageResource(R.mipmap.ic_empty);
                    i2.setImageResource(R.mipmap.ic_empty);
                    i3.setImageResource(R.mipmap.ic_empty);
                    i4.setImageResource(R.mipmap.ic_empty);
                    i5.setImageResource(R.mipmap.ic_empty);
                    i1.setColorFilter(redFilter);
                    i2.setColorFilter(redFilter);
                    i3.setColorFilter(redFilter);
                    i4.setColorFilter(redFilter);
                    i5.setColorFilter(redFilter);
                } else {
                    if (p > 4) {
                        i5.setImageResource(R.mipmap.ic_check);
                        i5.setColorFilter(blueFilter);
                    }
                    if (p > 3) {
                        i4.setImageResource(R.mipmap.ic_check);
                        i4.setColorFilter(blueFilter);
                    }
                    if (p > 2) {
                        i3.setImageResource(R.mipmap.ic_check);
                        i3.setColorFilter(blueFilter);
                    }
                    if (p > 1) {
                        i2.setImageResource(R.mipmap.ic_check);
                        i2.setColorFilter(blueFilter);
                    }

                    if (p > 0) {
                        i1.setImageResource(R.mipmap.ic_check);
                        i1.setColorFilter(blueFilter);
                    }
                }
            }
        });

    }
}